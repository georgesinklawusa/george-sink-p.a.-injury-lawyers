We are a personal injury law firm with locations all across South Carolina and Georgia. We are proud to represent car accident and injured victims who are not receiving their due compensation from the big insurance companies. George Sink and his team of attorneys will fight for you.

Address: 219 North US Highway 52, Suite R, Moncks Corner, SC 29461, USA

Phone: 843-407-0300
